/**
 * Created by jeffe on 19/05/2017.
 */


function validaDescricao(){
    var desc = document.getElementById("descricao");
    var notificacao = document.getElementById("notificacao");
    if(desc.value.length < 6){
        notificacao.style="visibility: visible; color: red; font-weight:bold";
        notificacao.textContent="A descrição deve ter ao menos seis caracteres!";
        return;
    }
    if(desc.value.substring(0, 1)==desc.value.substring(0, 1).toLocaleLowerCase()){
        notificacao.style="visibility: visible; color: red; font-weight:bold";
        notificacao.textContent="O primeiro caractere da descrição deve ser maiúsculo!";
        return;
    }
    document.getElementById("botao").type="submit";
}
