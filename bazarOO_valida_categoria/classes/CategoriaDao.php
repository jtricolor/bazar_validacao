<?php
require_once( BASE_DIR . "/classes/Banco.php");

trait CategoriaDao
{
  public static function rowMapper($idCategoria, $descricao, $taxa)
  {
    return new Categoria( $idCategoria, $descricao, $taxa);
  }

  public static function findAll()
  {
      $pdo = Banco::obterConexao();
      $statement = $pdo->prepare("SELECT idCategoria,descricao,taxa FROM Categoria");
      $statement->execute();
      /*
      return $statement->fetchAll( PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
      "Categoria", array( 'xxxx', 'xxx', 'xxx') );
      */

      return $statement->fetchAll( PDO::FETCH_FUNC, "CategoriaDao::rowMapper" );
  }

  public static function add( Categoria $categoria)
  {
	$pdo = Banco::obterConexao();
	$statement = $pdo->query("SELECT max(idCategoria) as maior_id FROM Categoria");

	$registro = $statement->fetch();

	$maior_id = $registro["maior_id"];

	if( $maior_id == null ) $novo_id = 1;
	else $novo_id = $maior_id + 1;

	$insere = $pdo->prepare("insert into Categoria (idCategoria, descricao, taxa) values (:id, :descricao, :taxa)");
	$insere->bindParam( ":id", $novo_id, PDO::PARAM_INT);
	$insere->bindParam( ":descricao", $categoria->getDescricao(), PDO::PARAM_STR);
	$insere->bindParam( ":taxa", str_replace( ',','.', $categoria->getTaxa() ), PDO::PARAM_STR);
	return $insere->execute();
  }
}


$pdo = Banco::obterConexao();
